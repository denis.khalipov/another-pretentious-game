﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
public class MenuBoll : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (GameController.Instance.StayGame == GameController.Stay.dontTouch)
        {
            Destroy(gameObject);
            GameController.Instance.StartGame();
            
            var effect = Instantiate(GameController.Instance.GetEffect, transform.position, Quaternion.identity);
            effect.GetComponent<SpriteRenderer>().color = gameObject.GetComponent<SpriteRenderer>().color;
            effect.transform.DOScale(10, 0.7f).OnComplete(delegate () { Destroy(effect); });
            effect.GetComponent<SpriteRenderer>().DOFade(0, 0.7f);
        }
        else
            SceneManager.LoadScene(0);
    }
}
