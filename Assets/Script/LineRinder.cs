﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LineRinder : MonoBehaviour
{
    public GameObject linePrefab;
    public GameObject currentLine;
    public LineRenderer lineRenderer;
    public EdgeCollider2D edgeCollider;
    public List<Vector2> fingerPositions;
    private List<Vector2> AllPosFinger;
    private float Timer;
    [SerializeField] private bool Menu = false;
    // Use this for initialization
    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            CreateLine();
            if (!Menu)
            {
                GameController.Instance.Chesc();
               
            }
            GameController.Instance.StayGame = GameController.Stay.Touch;
        }
        if (Input.GetMouseButton(0))
        {
            Timer += Time.deltaTime;
            StopAllCoroutines();
            Vector2 tempFingerPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (Vector2.Distance(tempFingerPos, fingerPositions[fingerPositions.Count - 1]) > .05f)
            {
                UpdateLine(tempFingerPos);
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
           
                GameController.Instance.StayGame = GameController.Stay.dontTouch;
            StartCoroutine(MoveLine());
        }
      



    }
    private IEnumerator MoveLine()
    {
        Debug.Log("fsdfdsf");
        AllPosFinger = fingerPositions;
        Vector2 stap = fingerPositions[fingerPositions.Count - 1] - fingerPositions[0];
        
        while (true)
        {
            
            for (int i = 0; i < fingerPositions.Count; i++)
            {
                // lineRenderer.SetPosition(i, lineRenderer.GetPosition(i) + stap);
                Vector2 firstPoint = AllPosFinger[0] + stap;
                if (i > 0)
                {
                    AllPosFinger[i - 1] = AllPosFinger[i];
                }

                if (i == fingerPositions.Count - 1)
                {
                    AllPosFinger[i] = firstPoint;
                }
               
            }
            for (int i = 0; i < AllPosFinger.Count; i++)
            {
                lineRenderer.SetPosition(i, AllPosFinger[i]);
            }
            edgeCollider.points = fingerPositions.ToArray();
           
          yield return new WaitForSeconds(Time.deltaTime);
        }
    }
    void CreateLine()
    {
        if(currentLine != null)
        {
            Destroy(currentLine);
        }
        currentLine = Instantiate(linePrefab, Vector3.zero, Quaternion.identity);
        lineRenderer = currentLine.GetComponent<LineRenderer>();
        edgeCollider = currentLine.GetComponent<EdgeCollider2D>();
        fingerPositions.Clear();
        fingerPositions.Add(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        fingerPositions.Add(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        lineRenderer.SetPosition(0, fingerPositions[0]);
        lineRenderer.SetPosition(1, fingerPositions[1]);
        edgeCollider.points = fingerPositions.ToArray();
    
    }
    void UpdateLine(Vector2 newFingerPos)
    {
        fingerPositions.Add(newFingerPos);
        if (fingerPositions.Count > 250)
        {
            fingerPositions.Remove(fingerPositions[0]);
        }
        lineRenderer.positionCount = fingerPositions.Count;
        for (int i = 0; i < fingerPositions.Count; i++)
        {
           
                lineRenderer.SetPosition(i, fingerPositions[i]);
            
        }
        edgeCollider.points = fingerPositions.ToArray();
       
    }
  
   
}
