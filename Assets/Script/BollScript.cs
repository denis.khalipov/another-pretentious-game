﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
public class BollScript : MonoBehaviour
{
    bool active = false;
    SpriteRenderer sp;
    CircleCollider2D CC;
    
    void Start()
    {
        GameController.Instance.AddBolls = gameObject;
        CC = GetComponent<CircleCollider2D>();
        sp = GetComponent<SpriteRenderer>();
        sp.color = GameController.Instance.GetRandomColor;
    }
    void Update()
    {
      
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (GameController.Instance.StayGame == GameController.Stay.dontTouch)
        {
            GameController.Instance.DelBoll = gameObject;
            sp.enabled = false;
            CC.enabled = false;
           var effect = Instantiate(GameController.Instance.GetEffect, transform.position, Quaternion.identity);
            effect.GetComponent<SpriteRenderer>().color = sp.color;
            effect.transform.DOScale(5, 0.7f).OnComplete(delegate() { Destroy(effect); });
            effect.GetComponent<SpriteRenderer>().DOFade(0, 0.7f);
        }
        else
            GameController.Instance.RestartAll();
    }
}
