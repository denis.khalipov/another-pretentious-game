﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleMobileAds.Api;

public class AdmobController : Singleton<AdmobController>
{
    private InterstitialAd AdInterstitials;
    public bool Active;
    void Start()
    {
        DontDestroyOnLoad(this);

#if UNITY_ANDROID
        string appId = "ca-app-pub-9060649143835901~1711073668";
#elif UNITY_IPHONE
            string appId = "ca-app-pub-9060649143835901~1140228022";
#else
        string appId = "unexpected_platform";
#endif

        MobileAds.Initialize(appId);

        RequestInterstitial();
    }

    private void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = "ca-app-pub-9060649143835901/6698858228";
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-9060649143835901/2927613104";
#else
        string adUnitId = "unexpected_platform";
#endif

        // Initialize an InterstitialAd.
        this.AdInterstitials = new InterstitialAd(adUnitId);
        AdRequest request = new AdRequest.Builder().Build();
        this.AdInterstitials.LoadAd(request);
    }
    public void ShowInterstitialsVideo()
    {
        if (Active)
        {
            if (AdInterstitials.IsLoaded())
            {
                AdInterstitials.Show();
                RequestInterstitial();
                Active = false;
            }
        }
        else if (!AdInterstitials.IsLoaded())
        {
            RequestInterstitial();
        }

    }
    private void Update()
    {
       
    }

}
