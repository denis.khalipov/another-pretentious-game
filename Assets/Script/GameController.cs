﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
public class GameController : Singleton<GameController>
{
    public enum Stay {Defauld,Touch,dontTouch}
    public Stay StayGame = Stay.dontTouch;
    private List<GameObject> Bolls = new List<GameObject>();
    [SerializeField] private GameObject[] Levels;
    private GameObject CurrentLevel;
    private int CountMaxBolls;
    private int Currentlevel;
    [SerializeField] private Text LvlText;
    [SerializeField] private Color[] RandomColor;
    [SerializeField] private bool Menu = false;
    [SerializeField] private Image HideGround;
    [SerializeField] private GameObject EffectBoll;
    public GameObject GetEffect
    {
        get { return EffectBoll; }
    }
    public Color GetRandomColor
    {
        get { return RandomColor[Random.Range(0, RandomColor.Length)]; }
    }
    public GameObject DelBoll
    {
        set { Bolls.Remove(value); }
    }
    public GameObject AddBolls
    {
        set { Bolls.Add(value);
            CountMaxBolls++;
        }
    }
   
    void Start()
    {
        HideGround.DOFade(0, 0.5f).OnComplete(delegate() { HideGround.gameObject.SetActive(false); AdmobController.Instance.ShowInterstitialsVideo(); });
       
        if (!Menu)
        {
            Currentlevel = PlayerPrefs.GetInt("Levels", 0);
            CurrentLevel = Instantiate(Levels[Currentlevel]);
            if (LvlText != null)
                LvlText.text = "Level:" + (Currentlevel + 1).ToString();
        }
        
    }

    public void Chesc()
    {
        if (Bolls.Count < CountMaxBolls)
        {
            Destroy(CurrentLevel);
            CountMaxBolls = 0;
            Bolls.Clear();
            CurrentLevel= Instantiate(Levels[Currentlevel]);
        }
    }
    void Update()
    {
        if (!Menu)
        {
            if (Bolls.Count <= 0 && GameController.Instance.StayGame == GameController.Stay.dontTouch)
            {
                AdmobController.Instance.Active = true;
                Currentlevel++;
                if (Currentlevel > Levels.Length - 1)
                {
                    Currentlevel = 0;
                }
                PlayerPrefs.SetInt("Levels", Currentlevel);
               
                SceneManager.LoadScene(1);
              
                
            }
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            Currentlevel++;
            if (Currentlevel > Levels.Length - 1)
            {
                Currentlevel = 0;
            }
            PlayerPrefs.SetInt("Levels", Currentlevel);
            SceneManager.LoadScene(1);
        }
     
    }
    public void MenuButton()
    {
        HideGround.gameObject.SetActive(true);
        HideGround.DOFade(1, 0.5f).OnComplete(delegate () { SceneManager.LoadScene(0); });
       
    }
    public void StartGame()
    {
        HideGround.gameObject.SetActive(true);
        HideGround.DOFade(1, 0.5f).OnComplete(delegate () { SceneManager.LoadScene(1); });

    }
    public void RestartAll()
    {
        SceneManager.LoadScene(1);
    }
}
