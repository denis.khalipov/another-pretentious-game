﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class MoveKillObject : MonoBehaviour
{
    [SerializeField] private Vector3 MovePos;
    [SerializeField] private float Speed;
    [SerializeField] private bool Move;
    [SerializeField] private bool Rotate;
    // Start is called before the first frame update
    void Start()
    {
        if(Move && !Rotate)
        transform.DOMove(MovePos, Speed).SetEase(Ease.Linear).SetLoops(-1,LoopType.Yoyo);
        else if (Rotate)
        {
            transform.DORotate(MovePos, Speed).SetEase(Ease.Linear).SetLoops(-1, LoopType.Incremental);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
